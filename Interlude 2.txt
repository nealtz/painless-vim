# Interlude: How to Back Off Without Giving Up

Let's face it, there are going to be times where you are sick to the *eye teeth* of vim and all its stupid weird commands and modes and just want to do some actual *work*. I get that. Even now I do that from time to time. Sometimes it's nice to feel like you're back in control instead of the student of some crazy workaholic teacher. 

There are two ways to make yourself feel better in moments like this. The first is to just go ahead and fire up your favorite editor and get to work. Use the arrow keys, all those other shortcut keys that are integrated into your OS, and just live the good life of a person who has a tool they like.

Just don't blame me if moving around using the arrow keys suddenly feels *intensely* awkward, or if you keep typing `jjkj` in your text trying to move up and down. It happens. It can be a little disappointing, but it's part of growing up. In a way it feels kind of nice. You're getting good at vim and starting to see the power there, and starting to realize how limiting other editors can be. 

The second way to help yourself feel better is to see if your favorite editor has a "vim compatible" or "vim emulation" mode. Many of them do. Once you enable this mode you can use as much or as little of the vim stuff as you want while still being well and truly productive in an editor you've already learned how to use. This way you're not really giving up on learning vim, you're just using what you've learned to make you more productive in, say, IntelliJ or Sublime Text. 

Sublime Text, by the way, does a good job of implementing pretty much all of vim's commands [^noO], and as an added bonus to people who are tired of starting up in "normal" mode, it always starts you into insert mode. So, you still have access to all the quick motion features of vim, all the shortcuts and easy editing methods, with all the added bonuses of a modern editor and even--glory be-- *mouse support*.

Coincidentally, if you decide that vim isn't your cup of tea, you could do far worse than using [Sublime Text] as your new editor of choice. Its built-in [^disabled] "Vintage Mode" does a great impression of vim, and it takes a lot of vim-like ideals (quick motion, mouse-free *everything*, text-only configuration, nearly infinite extensibility and configuration) into the modern era, and adds on a metric ton of good ideas all its own. In fact, my quest to learn vim started as a desire to use Sublime Text more efficiently. 

And no, nobody is paying me to say any of this. 

[^noO]: With the odd exception of `O` to open a new line above your current line. 

[^disabled]: but disabled by default

[Sublime Text]: http://www.sublimetext.com/