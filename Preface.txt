#Preface

## Why? Why This?

Look, we've all been through this: someone says, "You should learn vim. I did, and now I'm 10000% faster at everything and people like me more." So you download the latest version for your OS and start it up and, hey look, there's that screen with a bunch of tildes down the side asking you to donate to kids in Uganda. Just like the last time you tried this. You know from experience that typing `:help` isn't going to be super helpful, because you'll get stuck in the help window and you'll never be able to get it to go away. You start typing and the terminal beeps at  you or says it's in some weird mode now or --if you're lucky-- starts putting letters on the screen at some point. Then it's a quick trip to Google to figure out how to get back *out* of Vim, and you shudder, vowing not to go back.

It doesn't have to be this way.

At its heart, Vim is about making life easier for developers. But most of the "beginners" advice you get sounds like there's a big trial by fire initiation that you have to pass before you can get to that programmer's nirvana. People tell you that the "only way" you'll ever get good at using the power of vim is to throw your mouse out the window, disable all the hotkeys you're used to, and just spend a few months doing nothing but learning how to use an editor. 

Bless their pointy little heads. It's true that once you're *good* at vim you will start to see all the system shortcuts and mouse shortcuts as inefficient. But when you're starting out they make it feel like you're suddenly in a foreign country where you don't speak the language. They mean well, but but that sort of initiation is exactly wrong for a newcomer. It's advice like that that is impeding vim, keeping it from being *the* editor that everyone uses[^Katz] .

[^Katz]:You and I aren't the only ones that feel this way. Even [very smart people](http://yehudakatz.com/2010/07/29/everyone-who-tried-to-convince-me-to-use-vim-was-wrong/) can get caught by the "throw the newbie in the deep end" problem.

## I'm a Stranger Here, Myself

The fact is, you *can* learn vim at your own pace and be fairly productive with it right out of the box. And that's what this book is about. If vim were a foreign country, I would be the guy who walks up to you in the hotel lobby and says, "Hey, how's it going? Just get here? I'm a stranger here myself." And then I would show you all the good restaurants and stuff, because I understand what you're going through. 

So instead of lecturing you on learning all the underlying `ex` and `vi` commands that you "have to know to *truly* experience the *raw power* of vim" I'm going to show you how to get in, get comfortable, and start branching out into bigger and better things. I'm going to take it as read that you're not 100% committed to leaving all other text editors behind.  Heck, I'd strongly suggest having a copy of Sublime Text (more on that later) available for the times where you're just sick to the eye teeth of vim. It happens! But come back to vim, because I plan to show you that vim isn't that hard, and that learning it can be *painless*.