# Searching and (More Importantly) Finding {#searching}

Vim wouldn't be what it is without a good search function built in to the editor. Vim's search functionality is super powerful and surprisingly intuitive…once  you get used to it. 

Before we get started, I want to point out that vim searches come in two flavors: **forward** and **backward**. For each *type* of search, you can do that search from your cursor to the *end* of the document (which is the **forward** style) or from your cursor to the *beginning* of the file (which is the **backward** style). In most cases, searches will wrap when they hit the end (or beginning) of the document. I'll list the search commands with the **forward** style first and then the **backward** style.

## Whole File Searches: `/` and  `?`

One of the most common types of searches you'll do is searching the entire document for a word or term. To start this kind of search hit `/` or `?` and your cursor will jump down to the command line on the bottom of the screen. Type what you're searching for and press `<enter>`. Vim will highlight all the occurrences of the word you typed, and position the cursor at the occurrence of the word closest to your cursor. You can then jump to the next match using the `n` button. Remember that if you started the search using `?` the "next" match will be the match closer to the *beginning* of the document, not the *end*. 

But what if you want to *reverse* the direction of your search? Let's say you hit `?` when you started your search, but you want to start moving toward the *end* of the file now? No problem. Pressing `N` (that's an uppercase N instead of a lowercase n) will jump to the *previous* match, meaning the closest match *against* your direction of search.

Confused yet? I strongly encourage you play with these searches for a few moments in the `searching.txt` file, looking for the [Illuminati's secret mind control word](https://en.wikipedia.org/wiki/Fnord#Definition_and_usage).

## Find Me Another One of These:  `*` and `#`

Sometimes you want to find the next instance of the word you're on. This is especially useful in programming, where you might be trying to find all the places where a certain function is called or some such tomfoolery. In `searching.txt` you might want to find another copy of a word like "fnord". In Vim this is simple: position your cursor on the word you want to find and press `*` or `#`. You can jump between matches using `n` and `N`, just like with the whole file searches. 

## Line Searching: `t/T` and `f/F`

There are times where you just want to jump a few words forward or back along a line without hitting `w` or `b` over and over again.  Once again, vim is ready, this time with not one but *two* types of searches. 

If you type `t`*x* (or `T`*x*, for a reverse search) your cursor will jump "to" the closest occurrence of *x* in the line you're on. However, this might not work the way you were expecting. This "to" search will drop your cursor one character *before* the letter you searched for, like so:


	 | I'll Be There For You/When the Rain Starts To Fall 
	-> tF 
	 I'll Be There| For You/When the Rain Starts To Fall

I don't know *why* vim does this, it just does. It's worth noting that when you do a reverse search using `T` your cursor will be positioned one character *ahead* of the letter you typed. Again, I don't know why. But at least it's consistent. 

Fortunately, you've also got the `f`*x* search method, which works the way you would expect:


	| I'll Be There For You/When the Rain Starts To Fall   
	->fF 
	I'll Be There |For You/When the Rain Starts To Fall

In both cases, you use `;` to jump to the next match and `,` to jump to the previous match, unless you've [remapped the comma key](#what-map), but we won't get to that for a couple more chapters yet.

A> ## Line Searches for Surgical Changes
A>
A> You can use the t/T and f/F searches to cleanly and dare I say, *painlessly* cut a phrase or sentence out of the middle of a large block of text. The `d` and `c` commands both accept a search (which is technically a [*motion*](#Operators_2) ) as part of the command, letting you do something like `cf.` to remove a sentence from the middle of a paragraph and replace it with a new one. Or you can do `ct.` if you don't want to re-type the period at the end of the sentence. I guess recycling dots saves virtual trees? 
A> And it's just as useful the other direction. For one reason or another I often find my cursor at the *end* of a sentence and decide I want it gone.[^badwriter] All I have to do is do a backwards line search for the first character of the sentence, which is facilitated by the fact that the first letter in a sentence is usually uppercase. If we go back to the "I'll be there for you" line we can do search `dFI` to remove that entire line in one fell swoop.

[^badwriter]:This is not because I'm a terrible writer. I hope.

## R…R…Regular Expressions?!?

If you are already a fan of regular expressions for text manipulation then this is a good section for you. If you're not sure what a `/reg(ular)\s?ex(p|pression)/` is than I would point you to some [excellent](http://www.amazon.com/gp/product/0596528124/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=0596528124&linkCode=as2&tag=httpnatedicco-20) [resources](http://www.regular-expressions.info/) on the topic, and probably suggest that you skip the rest of this chapter. It's not that I don't want you here! It's just that *learning* regular expressions isn't exactly *painless*. It's *valuable*, but it takes time and effort. 

W> ## Why vim, Why??
W> 
W> If you were hoping for nice, perl-compatible regular expressions it's time for you to abandon that hope. Vim uses it's own weird regex engine that is almost, but not quite PCRE and isn't exactly `sed` either. It's kind of a pain. But that's why I'm here. To remove that pain. Even if it kills me. And It might. It just might.

Vim does let you search and replace using regular expressions, or "regexes" as they're called by people who can never figure out how to pronounce "regexp". So, that's the good news. The bad news is that vim uses a regular expression engine unlike any you've ever seen.

It starts out normal enough. If you want to search and replace a word you simply type `:s` followed by the usual regular expression replacement syntax:

	:s/old/new

And the next instance of the world `old` will be replaced with the word `new`. Simple, right? Okay. Let's go one step further. Now you don't want the word `old` anywhere on the current line. In this case just put the letter `g` at the end of the string, which makes it a "global" search, although in this case a "globe" means "one line of text." Don't ask me. The point is you can remove all occurrences of the word "old" from a single line using this command:

	:s/old/new/g

So far, no big deal, right? Okay. Let's say that you don't want the word `old` anywhere in your document at all. Okay, no problem. We'll tell vim to search the *entire* document and sub in `new` for `old`:

	:%s/old/new/g

What is that percent sign doing there, you ask? Remember clear back in the beginning of the book where we re-loaded our `.vimrc` file using the command `:so %`? Then, as now, the percent sign means "this here current file". So in this case we're telling vim to replace every instance of `old` in this here current file.

Okay, now let's get tactical about it. Let's say that the word "old" is only *persona non grata* between lines 25 and 50 of the document. You can tell vim what range you want to search using the following `start,end` syntax:

	:s 25,50 /old/new/g

"Wait," you say. "I'm scared! I'm afraid that the changes I made are wrong and will ruin the world!" Vim understands. Sometimes you don't want vim to change every instance of the word `old` without getting your stamp of approval first. You can do that. All you have to do is add the letter `c` (for "confirm") to the end of the command, like so:

	:%s/old/new/gc

When you hit `Enter` Vim will jump to each instance of "old" and ask if you want to change it. If you press `n` it will leave the word alone. If you *confirm* the change by pressing `y` it will make the change. Handy, no?

#### Where it Gets Ugly

"This," you are no doubt saying to yourself[^good_at_regex], is no big deal. This is just regular old *regular expressions*, not something to be worrying about. Well, I was just easing you in. Here's where it gets ugly. 

[^good_at_regex]:Because you are good at regular expressions and feeling pretty good at vim as well right now.

If you are a fan of the expressions most regular, you don't waste too much time before you start putting parts of your search in groups and using those search groups to make your life better. Let's say you want to do something simple like find a string of six hexadecimal characters. In most modern regular expression parsers[^pcre-parsers] you would type something like:

 	 [0-9a-f]{6}

In other words "Match any character between 0 and 9, or a-f six times". This is basic stuff.

And here's what vim does odd. Instead of letting you just wrap a section in parentheses, you have to *escape* the parentheses, or else vim will search for them. **I know.** So if you wanted to run the search above using vim, you would write it thus:

  	\\[0-9a-f\\]\\{6\\}

And that's a *fairly simple* regular expression. When you start having nested brackets and "or" clauses (which use the pipe symbol `|` ) you can reach a point where it feels like half of your regex is backslashes. 

### You Have to Believe Vim is Magic!

Fortunately, there's hope. Vim hasn't been static in all the years since vi, and while the old vi regex syntax is still the norm, you can override it without too much work. Vim has two special search modes called `magic` and `very magic`. Magic searches behave a lot more like POSIX style regular expressions, while **Very Magic** searches are more or less what you're used to if you learned regular expressions using Perl, Ruby, Python etc.

To access the "magic" or "very magic" search modes, you just prefix your search with `\m` (for magic) and `\v` (for *very* magic) mode. As you might expect, both magic and very magic modes have their opposites, which are triggered by using an uppercase `M` and `V` respectively. It's worth knowing exactly what you're getting into with each search mode, which means it's just about time to break out a fancy ol' table.

{title="Vim Regex Modes"}
|Mode 	|`\v`	|`\m`	|`\M` 	| `\V`	|   
|---------------|-------|--------|-------|--------|
|Name	| Very Magic	| Magic	| *Not* Magic	| *Very* Not Magic	|  
|End of line	| `$`	| `$`	| `$`	| `\$`	|  
|Parentheses	| `( )`	| `\( \)`	| `\( \)`	| `\( \)`	|  
|Alternates	| `|`	| `\|`	| `\|`	| `\|`	|  
|Repeats	| `*`	| `*`	| `\*`	| `\*`	|  
|Any Character	| `.`	| `.`	| `\.`	| `\.`	|  
|Literal dot	| `\.`	| `\.`	| `.`	| `.`	|  

As you can see, magic mode is an odd mix of  not-magic and very magic, which makes it kind of an annoying set of rules to remember. Before we get into any suggestions, here's a basic rundown of how each mode works, in descending order of magicality: 

- **Very Magic**: In this mode *every character* other than letters, numbers and underscores are *special characters*, meaning they modify the search instead of being a search term.
- **Magic**: In this mode *some characters* are special characters and others aren't. While this is vim's default search mode, it's also the least consistent. 
- **Not Magic**: In this mode *few characters* are special characters. In general you will have to escape things like parentheses or brackets to give them special character powers. 
- **Very Not Magic**: In this mode *only the backslash* is a special character. All characters in the search string will be interpreted literally unless preceded by a backslash. 


A> ## Sympathy for Vim Regexes
A> 
A> Vim's less magic search modes may seem obtuse and arcane, but they have their uses. Remember that vim is a *programmer's* text editor, and it's not uncommon for programmers to be looking for instances of parentheses and curly braces in their code. The search for an inner function like `function(a,function(b){//TODO:WRITE A CALLBACK FUNCTION HERE});` in your javascript you'll find is much easier to write as a "Very Not Magic" regex than as a "Very Magic" one:
A>
A> ~~~~~~~~
A>	/\Vfunction(\a){//
A> ~~~~~~~~
A>
A> versus:
A>
A> ~~~~~~~~
A>	/\vfunction\(\a\)\{\/\/
A> ~~~~~~~~
A>
A> I'm not saying that you should always use `\V`, but you also shouldn't shun it.

There's quite a bit more to know about vim's built-in regular expression engine, but at this point I'm going to point you at vim's built-in help to learn about it. If you type `:h search-pattern` in normal mode you'll be ready to embark upon a whole world of reading about how searches are conducted in vim. 

And if you're still afraid of vim's built-in help system, well, that's what the next chapter of *this* book is all about!

[^pcre-parsers]:Generally called `PCRE` or "Perl Compatible Regular Expression" parsers.
