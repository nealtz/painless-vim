# Operators: Moving and Changing at the Same Time  {#Operators_2}

So, now that you know all about counting and Operators and all the nuances of moving the cursor around, we're going to solidify our understanding of how motions and operators can work together.  

## Move-and-Edit Commands; or "Vim Commands 202"

Okay, we're going to re-hash a little here. A vim *command* consists of:

- An *operator*
- A *count*
- A *motion*

Depending on the situation, all three are optional. If you type `4` by itself vim will wait for you to tell it what to do four times. So you can give it a motion, like `j`, and it will move the cursor down four lines. Or you can give it an operator like `p` to paste something four times. Or you can give it both an operator and a motion, like `dl` to delete the next four characters. 

The last one is an example of *editing by motion*. Deleting a bunch of lines at once is just the tip of the iceberg. The great thing about learning to edit in this way is that once you've got the hang of it you can combine commands to make any change you want. Let's take a look at something I'm doing at work right now: Writing a `Gruntfile.js` configuration script for the [Grunt](http://gruntjs.com) automation system. 

What Grunt does is immaterial to the discussion; for now all we need to know is that there's a section that looks like this:

{lang="js"}
~~~~~~~~
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
~~~~~~~~

And so on for a bunch of lines. Now, I could have entered each of these lines by hand, but that's just stupid and painful and pointless. Vim made it much faster. The nice thing is that there are a couple of fast ways to do this, depending on how much thinking ahead you were able to do.  Let's look at some repetitive editing tasks, from the least forethought to the most.

### Least Forethought: Copy this line

So, let's say I typed the first line, and realized that I'm going to add another line just like it. I start out with this:

{lang="js"}
~~~~~~~~
grunt.loadNpmTasks('grunt-contrib-jade');
~~~~~~~~

And with my cursor at any point on that line I type `yyp`. In other words "yank the entire line and paste it." Now I have

{lang="js"}
~~~~~~~~
grunt.loadNpmTasks('grunt-contrib-jade');
grunt.loadNpmTasks('grunt-contrib-jade');
~~~~~~~~

Which is close to what I want. I can get my cursor to the word `jade` in the second line, type `ciw` and replace it with the word `watch`. There! I've added a new line. What's more, I can just type `p` and I'll have a third line that I can modify in the same way. I can keep doing this until I've got all the modules I need listed in my `Gruntfile`. 

### Slightly More Forethought: Copy this line. **A LOT**.

Okay, so let's go back to square one: I've got the first line written, and I realize that I'm going to need more than one copy of the line. No problem. But since I'm thinking ahead this time I'm not going to copy the word `jade` because I'm just going to change it in all of the copies. So I'll delete the word right now, giving me 

{lang="js"}
~~~~~~~~
grunt.loadNpmTasks('grunt-contrib-');
~~~~~~~~

This has the added side-benefit of throwing an error if I try to run grunt, since that's not a real module. A little added error checking is a good thing, I say. 

I'm thinking ahead, so instead of just hitting `p` to give myself a new copy I hit `7p` and have seven new lines ready for me to edit. Not bad! Now, I realize that sometimes you don't *know* how many nearly-identical lines you'll need, but we'll pretend it's more common than it is, because otherwise the next section is *entirely* pointless. 

### Uncanny Levels of Forethought: Knowing how many copies you want *Before you start typing*

Let's say that you know, right this moment, that you'll need 8 copies of a line that you haven't even typed.  Good for you! You can do this all in one step, and here's how. Ready? Watch close, this'll go by fast:

	8i grunt.loadNpmTasks('grunt-contrib-');<enter><esc>

And you're there. You can see that all we did was put a count (namely, *8*) before the "enter insert mode" operator `i`. So we told Vim to re-do anything we do in insert mode eight times. This is awesome, no? 

Again, I realize that it's not always what you want to do, nor is it even OFTEN something you want. But knowing about it is useful for two reasons: one, you can pull this trick when you need it, and two, you can figure out what just happened when you pull a trick like missing the `G` key and instead of *going* to line 55 and typing something  you type something right where you are, then copy it 55 times. Not that I've ever done that. Today. In this chapter. 

*Cough* 

What?


### The Triumphant Return Of The Almighty Dot

Okay, but here's the most useful edit-by-motion command you'll ever see *ever*. Let's say you've got a file where the indentation is all messed up, and there are entire methods that are over-indented. In the old days (assuming you weren't using Sublime Text in the old days) fixing this involved using the *mouse* to *highlight* all those lines and then hit `shft-tab` to un-indent that method. Vim can do you one step better. If you can quickly count all the lines you want to operate on at once, you can just put that number in front of the `<<` command and all your lines are moved back one tab, all together. 

"But Nate," you're saying, "The dot command wasn't in there *at all*". You're right, grasshopper. I was just easing you in. What's the other most common thing you have to do to a whole freaking boatload of lines all at once? That's right, it's *indenting* them. [^Kidding] Let's say you make the edit to a single line and realize that you're going to have to do this over and *over*. Don't. Move your cursor to the first line in the set that still needs edited and type `8.` 

In other words "make that last change on the next 8 lines." Suddenly anything you can do once you can do over and over again, without even thinking about it all that hard. It's kinda fun.

It's also a good reminder about our advice earlier about making *small* changes. The smaller a change is the easier it is to repeat and the more likely it is to be worth repeating.

[^Kidding]: I'm just kidding. Kind of. You could also be adding a comma to the end of every line in an array or list of attributes. 
