# Using Vim's Help System {#help}

So, clear back in the introduction I spoke about how the help system is a pain to use. And for a newcomer it kinda is. But like everything in vim it's only irritating while you're learning to use it. Once you've mastered it you'll find the help system incredibly useful and almost *intuitive* even. 

Okay, the basics: When you type `:help` in normal mode you'll suddenly have your vim screen split in half, the top half showing you a help file, the bottom half showing whatever you were already working on. Your cursor is up in the help window, and all your motion commands work like they always have: you can go up and down by half or whole pages, jump to the top or the bottom, the works. Which is nice and all, but the first question that most people have with when faced with this screen is "how do I make it go away?" That's a good question. Without any fancy wordplay, here's the answer: press `:q` and it's gone. You're back to your editor.[^q_is_not_quit]

[^q_is_not_quit]:This should remind you that `:q` isn't `quit` the way you would normally think of that verb. In fact, this is (yet another) good reason to use `ZZ` instead of `:qs` and `ZQ` instead of `:q!`; it breaks the mental link between the command and the thought of "quitting" vim. 

Okay, so, like we did earlier, let's start the help program up again and see what we can do with it. When you type `:h` you should notice that there are a bunch of words that are highlighted, like *links* from one part of the text to another, *hyper-links*, if you will. You're right, that's exactly what they are. Position your cursor on one of those links and type `<ctrl>]` to jump to the section referenced by that link. In reality, the help viewer is just showing you a specially formatted text file, coloring and centering and linking things according to the arcane rules of vim help files. If you ever write a vim plugin you would do well to learn this language, but for now we're going to ignore it. The other half of the help magic is that it's opening a new *window*, a concept that we'll cover [soon enough](#windows). For now all you need to know is that you can jump from window to another by typing `<ctrl>w` and a direction. Since the help window opens above your editor window you type `<ctrl>w j` and your cursor will jump down to your editor window. Type `<ctrl>w k` and you're back in the help window. 

## Getting the Help You Need

Vim's help system can only be *helpful* if you know how to ask it for things. The maintainers of the vim docs have thoughtfully separated out the help topics based on the *mode* in which you can use them, and vim makes it easy to search in this way. I could include a list of the prefixes you need to use to view the various sections of the help docs, but why reinvent the wheel? Vim can do that all by itself; in fact, if you moved around a bit when you were looking at the help documents earlier you probably already saw the document we're looking for. 

If not, let's go to the documentation now. In normal mode hit `:h` and find the line that says "get specific help" and place your cursor on that line. Now type `[zt](#zoom)` and  you should see *exactly* what we're looking for: A list of the prefixes you can use to view commands in insert mode, visual mode, options, and all the rest. Let's look up an insert mode command, just because we can. The help search function isn't case sensitive; so you can type everything lowercase and it'll work just fine. Let's look up `<ctrl>-r` because that's an insert mode function that has a lot of functionality. In normal mode just type 

	:h i_ctrl-r

And you've got some useful information about how to use [registers](#registers) in insert mode. Hit `ZZ` to close the help widow and let's keep playing!

T> ## Underscore vs. Hyphen
T>
T> Notice that you use an underscore between `i` and `ctrl` but a hyphen between `ctrl` and `r`. This isn't an accident; the underscore lets you know that you're searching by *mode*, the hyphen signifies that you're searching for a *key combination*. 

## Relatively Shallow Secrets of the Help System

It shouldn't surprise you that the vim help files are just a collection of plain text files[^nosurprise]. When you type `:h [something]` what you're doing is firing up vim's built in help file search engine. It doesn't (by default) do a full text search; it searches according to the specially formatted tags at the head of each section. But you can use it in other ways as well.

[^nosurprise]: It shouldn't surprise you because I told you that at the beginning of the chapter.

For example, you can search for a vim help file using its full name. Go ahead and type `:h insert.txt` and you'll see the full insert mode help document, with it's own table of contents and everything. It's worth noting that *sometimes* you can search for things without the extension and end up in the same place. For example, if you enter `:h cmdline` you'll get the `cmdline.txt` file, just as if you'd typed `h:cmdline.txt`.  However, try the same thing with "insert" and you'll get different results. Go ahead and try it: type `:h insert` without the `.txt` extension, and you'll still end up in the insert mode help file, but instead of being put at the table of contents you'll end up in "chapter 8: Insert mode commands". This is usually what you want when you're just looking for help on "insert", but it illustrates the point: if you search using the `.txt` file extension you'll be take to the top of the file, if you search without a file extension you'll be taken to what vim considers the best match for your search term. 

The open nature of vim help files makes it easy for plugins to include their own help files, and most of them do. If you installed pathogen in the [Plugins](#plugins) chapter you can type `:h pathogen<enter>` and see the help file for that plugin. 

So this is all well and good. But what if you don't *know* what you're looking for? Not to worry. You can just type `:h` and the word you're looking for, then instead of hitting `<enter>` press `<ctrl>d` and you'll get a list of possible topics that match what you've entered so far. Once you've found something that looks like what you want you just finish typing the term and you're on your way.

For example, let's say you're looking for help on the `yank` command, but you can't remember what it's called. You just know that it starts with a "y". Okay, we can handle that. Type `:h y<ctrl>d` and vim will give you a list of topics that contain the letter y. Which is an insanely huge list. If you can remember the next vowel (that's an "a" for those of you following along at home) you can narrow it down to the exact term you want. 

Okay, that wasn't the best example. But when you're trying to search for the help file included with some of the punnishly-named plugins like "fugitive" and "pathogen" just remembering that you're looking for "something about *git*" (and therefore typing `:h git<ctrl>d`) or "something that modifies *paths*" (and typing `:h path<ctrl>d`) will get you where you're going surprisingly quickly. 