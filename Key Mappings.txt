# Mapping Keys {#mapping-keys}

One of the good things about Vim is that it has a *lot* of commands, and pretty much every [plugin](#plugins) brings a bunch more. One of the bad things about vim is that it has a *lot* of *hard to type* commands, and pretty much every plugin brings a bunch more. Fortunately, it's easy to create keyboard shortcuts (or "key maps" in the vernacular) for these commands, thus freeing up your brain space and cutting down what you have to type into just a few characters.

However, this is definitely a topic where the Spider-Man warning applies[^spiderMan], because you can get yourself into trouble changing too much without thinking ahead. We're going to go over some common-sense strategies for mapping keys in vim, and set up a few maps that will help you work faster and more happily. Adding a new key map in vim is a matter of adding a single line to `.vimrc`, and once you get used to the syntax it's not terribly difficult. The problem is the syntax. Like many things in vim, it's concise, compact, and aggressively opaque unless someone tells you how to use it. That's why I'm here!

## All the Key map Commands

Since vim is a modal editor, it allows you to create key maps for each mode individually. The upshot is that you could have (this is **just an example**. Don't do this) `<ctrl>s` mean "save" in normal mode, "search" in insert mode, and "select all" in visual mode [^butdont]. 

You specify the target mode for your key map with the first letter of the `map` command:

- **nmap** : 
  normal mode
- **vmap** :
  visual mode
- **imap** :
  insert mode

Let's take a look at how you would use these in real life.

W> ##Don't Try This at Home!
W> 
W> Remember how I said that I would never tell you to change your configuration, even temporarily, in a bad way? Well, the examples I'm going to show here are ones you should read and understand, but *not* implement in your `.vimrc` file. We'll get to some useful examples farther on, for now I'm just showing you the pattern.

The pattern for new mappings is *map-mode* *keys* *command*. Let's say that you want to *always* append text to the end of a word when you jump to the end of a word. In stock vim you jump to the end of a word by just pressing `e` in normal mode. To enter insert mode we would add this line to `.vimrc`:

	nmap e ea

And then either restart vim or do our old `:so %` trick to reload our settings immediately. Now, when you press `e` your cursor jumps to the end of the word and enters insert mode, all at once. But let's say we didn't want to overwrite a built-in vim command (smart move! But more on that soon). Let's say we want to turn `<ctrl>e` into our "jump to the end and append" command. In that case we need to let vim know that we're using `<ctrl>` in our keymap, and to do that, we wrap the keys we're going to press in angle brackets, like so:

	nmap <C-e> ea

In vim, putting the letter C with a dash and any other character inside angle brackets says "I'll hold down Ctrl and this other character at the same time"[^mappingAlt]. If you just typed

	nmap C-e ea

You would have to type an uppercase C, a dash, and a lowercase e in that order to activate the key map. 

Okay, so, we've got the basic idea of keymaps: Tell vim where to use our new shortcut, what the shortcut is, and what it should do. Simple, right? Right. But we're not going to leave it that way for long. Normal mode is a busy place, and there are very few keys on the keyboard you can map with impunity. Fortunately, Vim has provided a way for us to extend the number of keys we can use without stepping on any currently mapped commands, using the `<Leader>` key.

##  Take me to your `<Leader>` Key!

You're right, if you look at any keyboard you won't find a key with "leader" written on it. This is because vim allows you to decide what key you want to use as a preface to your custom keymaps. In our example above we mapped `<C-e>` to a command, but that's already got a function in vim. What we could have done is written our mapping like this

	nmap <Leader>e ea

And all we have to do is press… something and then e and we're good! But what the heck is `<Leader>`?

By default, `<Leader>` is mapped to backslash (`\`). For most people this is a super annoying key to press and makes your "shortcuts" irritating. So for years people have added the following line to `.vimrc`:

	let mapleader=","

Which sets the comma key as `<Leader>`. Once you've done that you can just press `,e` and you have a simple way to jump to the end of a word and edit it all at once[^justasquick]. 

Q> ##Didn't you say that just about every key on the keyboard is already mapped?
Q>
Q> Yep. And comma is no exception. If you do a linewise search using `t/T` or `f/F` the comma will take you to the previous match in that line. Most people feel that they can go without that functionality in favor of having a leader key that they can use.

If you forget what keys you have mapped you can type `:map` in normal mode and be utterly overwhelmed with text. We'll get back to that in a while. For now let's talk about things we *shouldn't* map.

## What not to map

Vim being what it is, you're free to map any function you like to any keyboard action you like, and make vim work exactly the way you've always wanted. You could decide that you don't like `h j k l` and map them to some keys you like better, like, say, `w a s d`.  But as my dad always told me when I was growing up:

> Just because you can doesn't mean you should.
>
>  --*Nate's Dad*

There are a few excellent reasons why you *shouldn't* remap any of the basic commands. The first one is what I call the  *wallpaper effect*[^wallpaper]. As we have already stated, just about every key on the keyboard has a pre-defined job in normal mode. Let's stick with the terribly bad example above and say that you want to remap the movement keys  `h j k l` (because those are hard to remember) to `w a s d` (because you're a gamer and use those all the time) . But if you do that  you've got to figure out what to do about the following four commands:

- Next word (**w**)
- Append to word (**a**)
- Substitute character (**s**)
- Delete (**d**)

You could map them over to `h j k l`, but what sense does that make? `w` for "next word" is an natural and well crafted mnemonic. `k` makes far less sense. So you could try to map "Next word" to `n`, but then you've got to find a place for "find next", and so forth. 

Which leads to the problem of *portability*. One of the main reasons for learning vim is that it's installed by default on just about every non-Windows computer ever, and once you know how to use vim you can use it edit files quickly over any SSH connection, or hop on anyone else's machine and be productive almost immediately. But while *vim* is everywhere, your `.vimrc` file is only on your machines. This means that any time you connect to a computer that you don't own you're going to be trying to remember how to use vim the regular way all over again. Your muscle memory is going to trip you up, because you'll be reaching for your custom keys instead of the standard ones. You could just get in the habit of hauling your `.vimrc` file everywhere you ever work, but this slows you down. 

The other side of that is having other people collaborate with you via SSH or using tmux. Any vim users will be instantly and inconsolably furious when all the keystrokes they try do the entirely wrong things. It'd be like when you try to use someone's Dvorak keyboard when you're used to QWERTY or vice versa. 

In general, you should approach any remapping of built-in, standard commands with caution. *Adding* a keymap for something you do regularly is probably not terrible, but it still makes you reliant on being at your own personal terminal. *Overwriting* a keymap for a standard function should only be done if you know there is no better option.


## What to Map {#what-map}

So what do you map? It seems like my advice is to leave vim in a pristine state, unchanged and trusting blindly in the wisdom of Bram and crew. 

Well, not entirely. There are a lot of things that you should map, things that make vim more useful without crippling you on other machines. Most of these, unsurprisingly, have to do with plugins. Plugins are awesome. They extend your functionality and have become the core of any good text editor. And when you add them to vim it makes sense to find the most useful parts of the plugin and map those functions to something you like. 

For example, let's take the [fugitive](https://github.com/tpope/vim-fugitive) plugin by Tim Pope. Fugitive is a git management plugin, and exposes a lot of its functionality through the `:Gstatus` command. You could just type `:Gstatus` from the command line every time you wanted to use this plugin, or you could give it a nice, memorable keymap. Let's choose the second option. Since this is a git plugin, the letter g seems appropriate, but we don't want to overwrite it's current functionality. `<Leader>` to the rescue! We'll add the following to `.vimrc`

	nmap <Leader>g :Gstatus<CR>

And now (if you've remapped leader to the comma key) we can just press `,g` and get all that git status goodness in two characters, and we did it all without harming vim's central functionality. 

The other benefit here is that you can go to vim on any other machine and do your work without having compromised your muscle memory for the basics. Chances are that if you're working on someone else's computer you're not using git, because…that just feels wrong somehow. You'd be signed in as them and your commit would show up under their name…it's like wearing someone else's underwear. And on that pleasant note let's talk about a useful, nay *essential* keymap that has to be done *outside* of vim itself.

A>## Caps Lock Must Die!  {#clmd}
A>
A> I may have done you one slight disservice up to this point in the book. You see, up until now you've been reaching for the `<esc>` key clear up there in the top left corner of the keyboard when you want to get back to normal mode. The fact is that you don't have to do that. You can press `<ctrl>[` and it has the same effect.
A> Which doesn't seem like the biggest savings in the world, but you can make it even better. 
A> 
A> Most vim users have long since realized that they don't *need* a caps lock key. Even Google has realized it; the current Chromebooks have a search button where traditional keyboards have the Caps Lock button. But if you're using vim you can put that key to good use. 
A> 
A> If you re-map Caps Lock to be an additional `<ctrl>` key you can get back to normal mode by hitting the caps lock key and `[`, which is far easier than reaching for the `<esc>` key, and it makes a **lot** of other vim commands easier as well. Unlike emacs, which uses all of the modifier keys (ctrl, alt, shift, etc.) with astonishing regularity, vim pretty much sticks to the control key. This is because vim uses *modes* to expand what you can do with the keyboard, whereas emacs just uses modifier keys. Neither one is wrong. But vim's way is better. 
A>
A> Remapping the Caps Lock key generally has to be done at the operating system level, and Google is a good way to find out how to accomplish it in your specific OS. But once you do it, I promise you'll never look back.

## `<Plug>`

Okay, so there's one more not-on-the-keyboard key that we have to deal with. Unlike `<Leader>`, you'll never have to map a key to `<Plug>`, but you'll see it come up from time to time, so we're going to go over it in a high-level fashion.

Without getting into the details, `<Plug>` is a way for plugins to make it easy for you to create shortcuts for their functions. For example, let's say you have a plugin with an awesome feature called `awesome_feature`. The plugin developer could add the following to their plugin:	
	
~~~~~~~~
noremap <unique> <Plug>awesome :awesome_feature <CR>
~~~~~~~~

And then you could map that to something you want by adding this to `.vimrc`:

~~~~~~~~
nmap <Leader>a <Plug>awesome
~~~~~~~~

To use their awesome feature from there on out.


## Recursive Mappings, and How to Avoid Them

One feature of vim keymaps is that they can include *other* keymaps that are recursively executed. Like any feature, this can be used poorly, or used well. 

### Terrible Keymap Recursion

Let's consider the case of "Bob", a person who exists entirely to make poor choices in examples. 

Bob decides that he wants to use `F2` to copy a line. So he adds this line to `.vimrc`:

	map <F2> yy

Bob is forgetting that you shouldn't remap standard commands, but that's because he hasn't read this book yet. Someone buy him a copy, quick! But as bad as this idea is, it still works; bob can now press F2 instead of typing `yy` to copy entire lines, if that's your idea of a good time. But he's not done yet!

Bob (who is a huge Star Wars fan) is writing the seminal work on Wookiee culture, and wants to create an abbreviation for the name of the Wookiee home world, Kashyyyk. Bob is also a huge fan of overly fancy mappings, so he creates the following abbreviation:

	abbr kk Kash<F2>yk


A> ## Hey! What's this `abbr` Thing?
A> 
A> 'abbr' is, as you might guess, an abbreviation. Vim lets you define abbreviations as well as keymaps. For the most part they behave the same way, with a few minor differences: Abbreviations trigger after you press space, replacing the letters you typed with the listed abbreviation. For example, if you create the abbreviation `iabbr teh the` then when you're in insert mode and accidentally type `teh` vim will wait until you press space before replacing it with the correctly spelled `the`. 

Can you guess what will happen when Bob types `kk ` in his editor? That's right, he'll get the actual word Kashyyyk, because vim treats the characters and keymaps in abbreviations as if you entered them yourself, and pressing `F2` in Bob's world gives you `yy`. Bob is to be congratulated for making his keymap totally gaudy, terrible, hard to read *and* two characters longer than it has to be. 

But Bob still isn't done doing stupid things! He really **really** likes the letter `y`, so he's going to modify his `F2` keymap like so:

	map <F2>y<F2>

So now when Bob presses `F2` vim will type the `y` character, then see the `F2` key, which tells it to type y and then the `F2` key…and so on, forever. Or until Bob presses `Ctrl-C` to kill the recursion. This will also turn his `kk` abbreviation put an infinite number of `y`s in the middle of Chewie's home world.

The point of all this is that recursive key maps are totally possible in vim, and you can use them more intelligently than Bob does. But even Bob's terrible use of recursion shows us a few useful things:

- If you include a keymap inside another keymap  you can change the underlying map and it will update the outer one as well. [^rewrites]
- Infinite recursion is entirely possible, but getting out of that state is fairly easy.

### A More Useful Example

So let's take the lessons we learned from Bob and use them more intelligently. Let's start with something you might actually want to map:

	imap <F5> <C-R>=strftime("%H:%M")<CR>

Don't worry too much about that command (but if you want to know more you can check out [the vim wiki](http://vim.wikia.com/wiki/Insert_current_date_or_time)). It inserts the current time in "hour:minute" format. So if I pressed `F5` right now it would put "12:58" right before my cursor position. 

Now let's say we want a simple abbreviation to write log entries. We would add another line like this to `.vimrc`

	iabbr logt <F5>:Entry

And now when we're typing in the log we can just type `logt` and a timestamped log entry will be created. Now let's say we want to change the timestamp format. All we have to change is our `imap` line:

	imap <F5> <C-R>=strftime("%c")<CR>

And our log entry goes from

	12:58: Entry

To 

	Sat Jun 29 12:58:32 2013: Entry

### Avoiding Recursion Altogether

But they can also be somewhat confusing when you create a keymap that recurses into another keymap that you didn't want or expect. Fortunately you can easily tell a keymap that you *don't* want it to parse any keymap it comes across and just type the characters you typed, thank you very much. To do this you just change your map from this:

	imap <Leader> d dd

To this:

	inoremap <Leader> d dd

This is true of all the other modes as well: `vnoremap`, `nnoremap` work in exactly the same way. As you probably guessed, the `nore` stands for "no recursion", and ensures that anything in the expansion side of the remap is left un-evaluated. In general, it's a recommended practice to use the no recursion versions, because they're "protected" against accidental remaps from  new plugins or other keymaps you may add later.	

[^butdont]:But *seriously* don't do that. It's just an example. 

[^spiderMan]:With great power comes great responsibility.

[^mappingAlt]:It's worth noting that you can map the Alt key in a similar way: the pattern is <M-`n`>, where `n` is whatever letter you want to press along with the alt key. However, we're going to stick with vim tradition and avoid the alt key as much as possible, lest people think we are emacs users.

[^justasquick]:Some of you may have noticed that it's as quick or quicker to just *type* `ea` as it is to type `,e`. That's part of why I told you not to map this one.

[^wallpaper]: I don't have wallpaper either, but you're probably familiar with the image: you push down a bubble in one place and it pops up somewhere else. Also, in general, wallpaper is *really ugly*. 

[^rewrites]: I've rewritten this sentence approximately thirty times and can't think of a better way to say that. 
